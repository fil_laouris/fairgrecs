/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package healthrecs;

import GUI.*;
import Variables.Variables;
import javax.swing.JFrame;

/**
 *
 * @author mariastr
 */
public class HealthRecs {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {


        JFrame frame = new JFrame("FairGRecs Dataset");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Create and set up the content pane.
        TabDemo demo = new TabDemo();
        demo.addComponentToPane(frame.getContentPane(), frame);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

}
