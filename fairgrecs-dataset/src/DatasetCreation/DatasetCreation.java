/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DatasetCreation;

import Similarities.CreateEducation;
import Similarities.CreatePsychoEmotional;
import Similarities.OveralSimilarity;
import Similarities.SimilarUsers;
import Variables.Variables;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author mariastr
 */
public class DatasetCreation {
    
    Variables var;
    
    public DatasetCreation(Variables var){
        this.var = var;
    }
    
    public void createDataset() throws FileNotFoundException, IOException {
        
        // Documents
       
        String output = var.getOutput();
        if(!output.endsWith("\\")){
            output = output + "\\";
        }
        /*
        XML_Parser xml = new XML_Parser();

        // Full path of the ICD-10 ontology xml file 'ICD-10.xml'
        //File icd10 = new File(var.getOntology());
        //String icd10Path = icd10.getAbsolutePath();
        xml.parseXMLFile(var.getOntology());

        System.out.println("Creating Documents ...");
        BuildDocuments bd = new BuildDocuments(xml, var.getStopwords());
        bd.setNumRated(var.getNumRated());
        bd.buildDocuments(var.getNumKeywords(), var.getNumDocs());
        List<Documents> docs = bd.getNodes();
        bd.printDocuments(output);
        
         // Patients
         
        System.out.println("Creating Patients ...");

        CreatePatient cp = new CreatePatient(var.getPatients());

        cp.ReadIllnessFile(var.getAdmissions());
        //The percentages for the three different groups
        double gA = var.getGroupSparse() / 100.0;
        double gB = var.getGroupRegular() / 100.0;
        double gC = var.getGroupDedicated() / 100.0;
        double groupAper = gA;
        double groupBper = gB;
        double groupCper = gC;

        //The ranges from which a randomly number will be choosen as a patient's total number of ratings, according to his/her group
        
        int rangeAmin = var.getSparseMin();
        int rangeAmax = var.getSparseMax();

        int rangeBmin = var.getRegularMin();
        int rangeBmax = var.getRegularMax();

        int rangeCmin = var.getDedicatedMin();
        int rangeCmax = var.getDedicatedMax();

        cp.divideGroups(groupAper, rangeAmin, rangeAmax, groupBper, rangeBmin, rangeBmax, groupCper, rangeCmin, rangeCmax);

        //The percentages of the number of ratings that a user will give which will be relevant with his/her health problems
        double hr = var.getHealthRelevant() / 100.0;
        double nhr = var.getNoHealthRelevant() / 100.0;
        double healthRelevant = hr;
        double noRelevant = nhr;

        cp.setRatingsSpan(healthRelevant, noRelevant);
        int totalRatings = cp.getTotalRatings();
       
        System.out.println("Creating Ratings Data...");
        double tail = 0.5;
        GenerateSyntheticRatings rat = new GenerateSyntheticRatings(cp.getPatients(), docs, bd.getCategoryLex());
        System.out.println("Finalize Rating Data");
        rat.setNumRating(totalRatings);

        //The percentages of the values of the ratings
        //double one = 0.2, two = 0.1, three = 0.3, four = 0.2, five = 0.2;
        double one = var.getScore1() / 100.0;
        double two = var.getScore2() / 100.0;
        double three = var.getScore3() / 100.0;
        double four = var.getScore4() / 100.0;
        double five = var.getScore5() / 100.0;
        rat.generateRating(one, two, three, four, five);
        rat.printRatings(output);
*/
        System.out.println("Creating Education Data...");
        CreateEducation edu = new CreateEducation();
        edu.openFile(var.getEducation());
        System.out.println("Calculate Education Similarities...");
        edu.CreateNewEducation(1000, var.getUserId());
        edu.CalculateSimilarities();
        edu.printEducationSimilarities(output);

        System.out.println("Creating AnxientyPschycoEmotion Data...");
        CreatePsychoEmotional anx = new CreatePsychoEmotional();
        anx.openFile(var.getPsychoEmotional());
        System.out.println("Calculate Emotional Similarities...");
        anx.CreateNewEmotion(1000, var.getUserId());
        anx.CalculateEmotionalSimilarities();
        anx.printEmotionSimilarities(output);

        System.out.println("Calculate total Similarities...");
        OveralSimilarity total = new OveralSimilarity();
        total.CalculateOveralSimilarities();
        total.printTotalSimilarities(output);

        System.out.println("Calcutate users ratings....");
        SimilarUsers su = new SimilarUsers(var);
        su.readUserRatings();
        su.fillRatings(var.getUserId(), var.getUserRatNum());
        su.calculateRatings(var.getUserId(), var.getTopRatNum());
        su.printRatings(output);
        su.OtherRatings();
    }
}
