/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Similarities;

/**
 *
 * @author f_lao_000
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import Variables.Variables;
/**
 *
 * @author f_lao_000
 */
class totalSimilarity {
    String id_1;
    String id_2;
    int similarity;
    totalSimilarity(String ID_1, String ID_2, int similarity) {
        this.id_1 = ID_1;
        this.id_2 = ID_2;
        this.similarity = similarity;
    }

    public static Comparator<totalSimilarity> totalComparator = new Comparator<totalSimilarity>() {
        public int compare(totalSimilarity o1, totalSimilarity o2) {
            int no1 = o1.similarity;
            int no2 = o2.similarity;

            return no1 - no2;
        }
    };
}

class similarUserRec {
    String id_1;
    String id_2;
    int weight;

    similarUserRec(String ID_1, String ID_2, int weight) {
        this.id_1 = ID_1;
        this.id_2 = ID_2;
        this.weight = weight;
    }
}

public class OveralSimilarity {
    ArrayList<EducationLevel> education = CreateEducation.getEducationList();
    ArrayList<psychoEmotionalLevel> emotion = CreatePsychoEmotional.getEmotionList();

    public static ArrayList<totalSimilarity> total = new ArrayList<totalSimilarity>();
    public static ArrayList<totalSimilarity> customTotal = new ArrayList<totalSimilarity>();
    Variables var;
    int count1 = 0;
    int count2 = 0;
    int count3 = 0;
    int count4 = 0;
    int count5 = 0;
    int count0 = 0;
    int other = 0;
    int count_overal = 0;
    int total_similarity = 0;
    public void CalculateOveralSimilarities() {
        int i = 0;
        while (i < education.size() && i < emotion.size()) {
            EducationLevel edu = education.get(i);
            psychoEmotionalLevel emo = emotion.get(i);
            total_similarity = (int) (emo.similarity + edu.similarity) / 2;
            switch (total_similarity) {
                case 0:
                    count0++;
                    break;
                case 1:
                    count1++;
                    break;
                case 2:
                    count2++;
                    break;
                case 3:
                    count3++;
                    break;
                case 4:
                    count4++;
                    break;
                case 5:
                    count5++;
                    break;

                default:
                    other++;
                    break;
            }
            count_overal++;
            totalSimilarity similarities = new totalSimilarity(emo.ID1, emo.ID2, total_similarity);
            total.add(similarities);
            i++;
        }
    }

    public static ArrayList<totalSimilarity> getOveralList() {
        return total;
    }
    public void printTotalSimilarities(String output) {
        String file = output + "totalSimilarities.txt";
        System.out.println("printSimilarities");
        try {
            PrintWriter writer = new PrintWriter(file, "UTF-8");
            writer.println("\tUSER_ID1\t\t\tUSER_ID2\t\t\tTOTAL_SIMILARITY");
            for (totalSimilarity overal : total) {
                writer.println(overal.id_1 + "\t" + overal.id_2 + "\t" + overal.similarity);
            }
            writer.close();
        } catch (IOException e) {
        }
    }
}
