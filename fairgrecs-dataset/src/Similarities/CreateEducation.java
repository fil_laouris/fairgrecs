/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Similarities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author f_lao_000
 */
class Education {

    String ID;
    int edu;
    int literacy;

    Education(String ID, int edu, int literacy) {
        this.ID = ID;
        this.edu = edu;
        this.literacy = literacy;
    }

}
class EducationLevel {
    String ID1;
    String ID2;
    int similarity;

    EducationLevel(String ID1, String ID2, int similarity) {
        this.ID1 = ID1;
        this.ID2 = ID2;
        this.similarity = similarity;
    }
}
class Users {
    String ID;
    int edu;
    int literacy;
    Users(String ID, int edu, int literacy) {
        this.ID = ID;
        this.edu = edu;
        this.literacy = literacy;
    }
}
public class CreateEducation {
    public static ArrayList<Education> education1 = new ArrayList<Education>();
    public static ArrayList<Education> education = new ArrayList<Education>();
    public static ArrayList<Users> user = new ArrayList<Users>();
    public static ArrayList<EducationLevel> educationlvl = new ArrayList<EducationLevel>();
    public static ArrayList<EducationLevel> customEducationlvl = new ArrayList<EducationLevel>();
    int count1 = 0;
    int count2 = 0;
    int eduSimilarity = 0;
    public void openFile(String fileName) throws FileNotFoundException {
        FileReader fileReader = new FileReader(fileName);
        try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            line = bufferedReader.readLine();
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.equalsIgnoreCase("")) {
                    continue;
                }
                String[] parts = line.split("\t");
                if (parts.length != 3) {
                    System.out.println("> " + parts[0]);
                }
                String part0 = parts[0];
                String part1 = parts[1];
                String part2 = parts[2];
                String ID = part0;
                int edu = Integer.parseInt(part1);
                int literacy = Integer.parseInt(part2);
                Education ed = new Education(ID, edu, literacy);
                Users us = new Users(ID, edu, literacy);
                user.add(us);
                education1.add(ed);
            }
            bufferedReader.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(CreateEducation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CreateEducation.class.getName()).log(Level.SEVERE, null, ex);
        }
        ArrayList<Education> users = (ArrayList<Education>) education1.clone();
    }

    public void CreateNewEducation(int num, String ID) {
        int i = 0, count = 0, temp = 0;
        String id = "";
        int edu = 0;
        int literacy = 0;
        ArrayList<Education> education1 = getEducation1();
        while (i < education1.size() && count < num) {
            if (ID.equals(education1.get(i).ID)) {
                while (count < num) {
                    id = education1.get(i).ID;
                    edu = education1.get(i).edu;
                    literacy = education1.get(i).literacy;
                    Education ed = new Education(id, edu, literacy);
                    education.add(ed);
                    i++;
                    count++;
                    temp = education1.size() - 1;
                    if (i == temp) {
                        i = 0;
                    }
                }
            }
            i++;
        }
    }

    public void CalculateSimilarities() {
        for (Education edu1 : education) {
            count1++;
            count2 = 0;
            for (Education edu2 : education) {
                count2++;
                //   if (count1 < coifunt2) {
               if (edu1.ID != edu2.ID) {
                    eduSimilarity = (int) Math.sqrt(Math.pow((edu1.literacy - edu2.literacy), 2) + Math.pow((edu1.edu - edu2.edu), 2));
                    EducationLevel edu_level = new EducationLevel(edu1.ID, edu2.ID, eduSimilarity);
                    educationlvl.add(edu_level);
                }
            }
        }
    }

    public static ArrayList<EducationLevel> getEducationList() {
        return educationlvl;
    }

    public static ArrayList<Education> getEducation1() {
        return education1;
    }

    public static ArrayList<Education> getEducation() {
        return education;
    }
    public static ArrayList<Users> getUsers() {
        return user;
    }
    public void printEducationSimilarities(String output) {
        String file = output + "educationSimilarities.txt";
        try {
            PrintWriter writer = new PrintWriter(file, "UTF-8");
            writer.println("\tUSER_ID1\t\t\tUSER_ID2\t\t\tEDUCATION_SIMILARITY");
            for (EducationLevel edu : educationlvl) {
                writer.println(edu.ID1 + "\t" + edu.ID2 + "\t" + edu.similarity);
            }
            writer.close();
        } catch (IOException e) {
        }
    }
}
