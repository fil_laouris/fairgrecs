/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Similarities;

import static Similarities.ratingDocs.ratUserComparator;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import Variables.Variables;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author f_lao_000
 */
class ratingDocs {
    String ID;
    int doc_id;
    int doc_rate;
    String keyword;

    ratingDocs(String ID, int doc_id, int doc_rate, String keyword) {
        this.ID = ID;
        this.doc_id = doc_id;
        this.doc_rate = doc_rate;
        this.keyword = keyword;
    }

    public static Comparator<ratingDocs> ratComparator = new Comparator<ratingDocs>() {
        public int compare(ratingDocs o1, ratingDocs o2) {
            int no1 = o1.doc_id;
            int no2 = o2.doc_id;
            return no1 - no2;
        }
    };

    public static Comparator<ratingDocs> ratUserComparator = new Comparator<ratingDocs>() {
        @Override
        public int compare(ratingDocs o1, ratingDocs o2) {
            String no1 = o1.ID;
            String no2 = o2.ID;
            int id_comp = no1.compareToIgnoreCase(no2);
            if (id_comp != 0) {
                return id_comp;
            } else {
                return Integer.valueOf(o2.doc_rate).compareTo(Integer.valueOf(o1.doc_rate));
            }
        }
    };
}

class similarUserRecs {
    String id_1;
    String id_2;
    int weight;
    similarUserRecs(String ID_1, String ID_2, int weight) {
        this.id_1 = ID_1;
        this.id_2 = ID_2;
        this.weight = weight;
    }
}

public class SimilarUsers {
    Variables var;

    public SimilarUsers(Variables var) {
        this.var = var;
    }
    static ArrayList<ratingDocs> ratDoc = new ArrayList<ratingDocs>();
    static ArrayList<ratingDocs> maxRatedDocs = new ArrayList<ratingDocs>();
    ArrayList<similarUserRecs> simUsers = new ArrayList<similarUserRecs>();
    public static String[] readKeywords() throws FileNotFoundException, IOException {
        String[] keywordArray = new String[500];
        File file = new File("runFiles/keywords.txt");
        int i = 0;
        FileReader fileReader = new FileReader(file);
        try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            line = bufferedReader.readLine();
            while ((line = bufferedReader.readLine()) != null && i < 500) {
                String[] parts = line.split("\n");
                if (parts.length != 1) {
                    System.out.println("> " + parts[0]);
                }
                String part0 = parts[0];
                String keyword = part0;
                keywordArray[i] = keyword;
                i++;
            }
            bufferedReader.close();
        }
        return keywordArray;
    }

    public static void readUserRatings() throws FileNotFoundException, IOException {
        int i = 0;
        File file = new File("runFiles/ratings.txt");
        FileReader fileReader = new FileReader(file);
        try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            line = bufferedReader.readLine();
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.equalsIgnoreCase("")) {
                    continue;
                }
                String[] parts = line.split("\t");
                if (parts.length != 3) {
                    System.out.println("> " + parts[0]);
                }
                String part0 = parts[0];
                String part1 = parts[1];
                String part2 = parts[2];
                String ID = part0;
                int doc_id = Integer.parseInt(part1);
                int rate_doc = Integer.parseInt(part2);
                String[] keywordArr = SimilarUsers.readKeywords();
                String keyword = keywordArr[doc_id - 1];
                if (keyword != null) {
                    ratingDocs rat = new ratingDocs(ID, doc_id, rate_doc, keyword);
                    ratDoc.add(rat);
                }
            }
            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CreatePsychoEmotional.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CreatePsychoEmotional.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static ArrayList<ratingDocs> getRatings() {
        return ratDoc;
    }
    public static ArrayList<ratingDocs> getMaxRatings() {
        return maxRatedDocs;
    }
    public int checkUserRec(String user_id, int doc_id) {
        ArrayList<ratingDocs> ratDocs = SimilarUsers.getRatings();
        int rate = 0;
        for (ratingDocs rat : ratDocs) {
            boolean id = user_id.equals(rat.ID);
            boolean doc = doc_id == rat.doc_id;
            if (id && doc) {
                rate = rat.doc_rate;
                break;
            }
        }
        return rate;
    }
    public int findMostSimilarUSersRate(int num, String user_id, int doc) {
        int user_rate, sum_rate = 0;
        int weight = 0;
        int users_with_rates = 0;
        ArrayList<totalSimilarity> total = OveralSimilarity.getOveralList();
        ArrayList<similarUserRecs> simUsers = new ArrayList<similarUserRecs>();
        int k = 0;
        totalSimilarity curr = total.get(k);
        while (k < total.size()) {
            boolean find = user_id.equals(curr.id_1);
            if (find) {
                break;
            }
            k++;
            curr = total.get(k);
        }
        if (curr.id_1.equals(user_id)) {
            int p = 0;
            while (p < num) {
                curr = total.get(k);
                similarUserRecs simU = new similarUserRecs(curr.id_1, curr.id_2, curr.similarity);
                simUsers.add(simU);
                p++;
                k++;
            }
            int j = 0;
            while (j < simUsers.size()) {
                similarUserRecs strCurrNum = simUsers.get(j);
                String id = strCurrNum.id_2;
                SimilarUsers simUs = new SimilarUsers(var);
                user_rate = simUs.checkUserRec(id, doc);

                if (user_rate != 0) {
                    users_with_rates++;
                    switch (strCurrNum.weight) {
                        case 0:
                            user_rate = user_rate * 1;
                            break;
                        case 1:
                            user_rate = (int) (user_rate * 0.8);
                            break;
                        case 2:
                            user_rate = (int) (user_rate * 0.6);
                            break;
                        case 3:
                            user_rate = (int) (user_rate * 0.4);
                            break;
                        case 4:
                            user_rate = (int) (user_rate * 0.2);
                            break;
                        case 5:
                            user_rate = (int) (user_rate * 0.1);
                            break;
                        default:
                            break;

                    }
                    sum_rate = sum_rate + user_rate;
                }
                j++;
            }
        }
        if (sum_rate == 0)
            return sum_rate;
        else
            return sum_rate / users_with_rates;
    }


    public void fillRatings(String userID, int num) throws IOException {
        ArrayList<Users> user = CreateEducation.getUsers();
        int i = 0;
        int doc = 1;
        while (doc <= 500) {
            i = 0;
            int rate = 0;
            SimilarUsers simU = new SimilarUsers(var);
            rate = simU.checkUserRec(userID, doc);
                if (rate == 0) {
                    rate = (int) (simU.findMostSimilarUSersRate(num, userID, doc));
                    String[] keywordArr = SimilarUsers.readKeywords();
                    String keyword = keywordArr[doc - 1];
                    ratingDocs rat = new ratingDocs(userID, doc, rate, keyword);
                    ratDoc.add(rat);
            }
            doc++;
        }
        }

    public void calculateRatings(String userID, int num) throws IOException {
        Collections.sort(ratDoc, ratUserComparator);
        int users = 1, i = 0, k = 0;
        ArrayList<Users> user = CreateEducation.getUsers();
        ratingDocs curr = ratDoc.get(k);
        ratingDocs curr_next = ratDoc.get(k + 1);
        int doc = 0, rate = 0;
        String id = "", keyword = "";
        ratingDocs old_curr = new ratingDocs(id, doc, rate, keyword);
        while (k < ratDoc.size() && !curr.ID.equals(userID)) {
            k++;
            curr = ratDoc.get(k);
        }
        curr_next = ratDoc.get(k + 1);
        while ((curr.ID).equals(curr_next.ID) && i < num) {
            String[] keywordArr = SimilarUsers.readKeywords();
            keyword = keywordArr[curr.doc_id - 1];
            curr.keyword = keyword;
            ratingDocs maxRate = new ratingDocs(curr.ID, curr.doc_id, curr.doc_rate, curr.keyword);
            maxRatedDocs.add(maxRate);
            k++;
            i++;
            curr = ratDoc.get(k);
            curr_next = ratDoc.get(k + 1);
            old_curr.ID = curr.ID;
        }
    }

    public void printRatings(String output) throws IOException {
        String file = output + "MAXRatingsUser.txt";
        ArrayList<ratingDocs> maxRatedDocs = SimilarUsers.getMaxRatings();
        try {
            PrintWriter writer = new PrintWriter(file, "UTF-8");
            writer.println("\tUSER_ID\t\t\t\tDOC_ID\tUSER_RATE\tKEYWORD");
            for (ratingDocs rat : maxRatedDocs) {
                writer.println(rat.ID + "\t" + rat.doc_id + "\t" + rat.doc_rate + "\t\t" + rat.keyword);
            }
             writer.close();
        } catch (IOException e) {
        }
    }

    public void OtherRatings() throws IOException {
        int res, trace = 0;
        ArrayList<Users> users = CreateEducation.getUsers();
        String[] keyword = SimilarUsers.readKeywords();
        String response = (String) JOptionPane.showInputDialog(null,
                "what else would you be interested in?", "Message",
                JOptionPane.QUESTION_MESSAGE, null, keyword, "AA (Alopecia Areata)");

        res = checkRatings(response);
        while (res <= 0) {
            JFrame f = new JFrame();
            JOptionPane.showMessageDialog(f, "the keyword " + response
                    + " you have entered already exists in the suggested documents ", "Alert", JOptionPane.WARNING_MESSAGE);
            f.dispose();
            response = (String) JOptionPane.showInputDialog(null,
                    "what else would you be interested in?", "Message",
                    JOptionPane.QUESTION_MESSAGE, null, keyword, "AA (Alopecia Areata)");
            res = checkRatings(response);
        }
        if (res > 0) {
            int total_users = 1000;
            String userID = var.getUserId();
            int num = var.getUserRatNum();
            int top = var.getTopRatNum();
            int error = 1;
            CreatePsychoEmotional anx = new CreatePsychoEmotional();
            CreateEducation edu = new CreateEducation();
            OveralSimilarity total = new OveralSimilarity();
            while (trace >= 0 && num < users.size() && top < ratDoc.size()) {
                total_users = total_users - 100;
                if (total_users > 0 && error == 0) {
                    edu.CreateNewEducation(total_users, var.getUserId());
                    edu.CalculateSimilarities();
                    anx.CreateNewEmotion(total_users, var.getUserId());
                    anx.CalculateEmotionalSimilarities();
                    total.CalculateOveralSimilarities();
                    num = num + 2;
                    fillRatings(userID, num);
                    top = top + 5;
                    calculateRatings(userID, top);
                    trace = checkRatings(response);
                } else {
                    error++;
                    edu.CreateNewEducation(total_users, var.getUserId());
                    edu.CalculateSimilarities();
                    anx.CreateNewEmotion(total_users, var.getUserId());
                    anx.CalculateEmotionalSimilarities();
                    total.CalculateOveralSimilarities();
                    while (trace >= 0 && num < users.size() && top < ratDoc.size()) {
                        num = num + 2;
                        fillRatings(userID, num);
                        top = top + 5;
                        calculateRatings(userID, top);
                        trace = checkRatings(response);
                    }
                }
            }

            JFrame f = new JFrame();
            res++;
            if (error == 0) {
            JOptionPane.showMessageDialog(f, "To include the file with keyword :" + response + " \n\n Raise the number of similar users:" + num
                        + "\n The number of given documents to:" + top + "\n Reduce number of users to: " + total_users);
            } else {
                JOptionPane.showMessageDialog(f, "To include the file with keyword :" + response + " \n\n Raise the number of similar users:" + num
                        + "\n The number of given documents to:" + top);
            }

            f.dispose();
        }
    }
    public int checkRatings(String keyword) throws IOException {
        int k = 0, i = 0;
        ArrayList<ratingDocs> ratDocs = SimilarUsers.getRatings();
        ratingDocs curr = ratDoc.get(i);
        ratingDocs max_curr = maxRatedDocs.get(k);
        while (k < maxRatedDocs.size()) {
            max_curr = maxRatedDocs.get(k);
            if ((max_curr.keyword.equals(keyword))) {
                return -1;
            } else {
                k++;
            }
        }
        while (i < (ratDoc.size() - 1)) {
            curr = ratDoc.get(i);
            if (curr.keyword == null) {
                System.out.println("vrika null" + i);
                return 0;
            }
            if ((curr.keyword).equals(keyword)) {

                return i;
            }
            i++;
        }
        return ratDoc.size();
    }
}
