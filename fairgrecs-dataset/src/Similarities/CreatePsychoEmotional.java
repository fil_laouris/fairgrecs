/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Similarities;
/**
 *
 * @author f_lao_000
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author f_lao_000
 */
class psychoEmotionalStatus {
    String ID;
    int anxienty;
    int cognClosure;

    psychoEmotionalStatus(String ID, int anxienty, int cognClosure) {
        this.ID = ID;
        this.anxienty = anxienty;
        this.cognClosure = cognClosure;
    }
}

class psychoEmotionalLevel {
    String ID1;
    String ID2;
    int similarity;
    psychoEmotionalLevel(String ID1, String ID2, int similarity) {
        this.ID1 = ID1;
        this.ID2 = ID2;
        this.similarity = similarity;
    }
}

public class CreatePsychoEmotional {
    public static ArrayList<psychoEmotionalStatus> psychoEmotion1 = new ArrayList<psychoEmotionalStatus>();
    public static ArrayList<psychoEmotionalStatus> psychoEmotion = new ArrayList<psychoEmotionalStatus>();
    public static ArrayList<psychoEmotionalLevel> emotionLevel = new ArrayList<psychoEmotionalLevel>();
    int count1 = 0;
    int count2 = 0;
    int emotionSimilarity = 0;


    public void openFile(String fileName) throws FileNotFoundException {
        FileReader fileReader = new FileReader(fileName);
        try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            line = bufferedReader.readLine();
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.equalsIgnoreCase("")) {
                    continue;
                }
                String[] parts = line.split("\t");
                if (parts.length != 3) {
                    System.out.println("> " + parts[0]);
                }
                String part0 = parts[0];
                String part1 = parts[1];
                String part2 = parts[2];
                String ID = part0;
                int anxienty = Integer.parseInt(part1);
                int cognClosure = Integer.parseInt(part2);
                psychoEmotionalStatus em = new psychoEmotionalStatus(ID, anxienty, cognClosure);
                psychoEmotion1.add(em);
            }
            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CreatePsychoEmotional.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CreatePsychoEmotional.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void CalculateEmotionalSimilarities() {
        for (psychoEmotionalStatus emo1 : psychoEmotion) {
            count1++;
            count2 = 0;
            for (psychoEmotionalStatus emo2 : psychoEmotion) {
                count2++;
                if (emo1.ID != emo2.ID) {
                    emotionSimilarity = (int) Math.sqrt(Math.pow((emo1.anxienty - emo2.anxienty), 2) + Math.pow((emo1.cognClosure - emo2.cognClosure), 2));
                    psychoEmotionalLevel emotion_level = new psychoEmotionalLevel(emo1.ID, emo2.ID, emotionSimilarity);
                    emotionLevel.add(emotion_level);
                }
            }
        }
    }

    public static ArrayList<psychoEmotionalLevel> getEmotionList() {
        return emotionLevel;
    }

    public static ArrayList<psychoEmotionalStatus> getEmotionList1() {
        return psychoEmotion1;
    }


    public void CreateNewEmotion(int num, String ID) {
        int i = 0, count = 0;
        String id = "";
        int anxiety = 0;
        int cognclosure = 0;
        while (i < psychoEmotion1.size() && count < num) {
            if (ID.equals(psychoEmotion1.get(i).ID)) {
                while (count < num) {
                    psychoEmotionalStatus em = new psychoEmotionalStatus(psychoEmotion1.get(i).ID, psychoEmotion1.get(i).anxienty, psychoEmotion1.get(i).cognClosure);
                    psychoEmotion.add(em);
                    i++;
                    count++;
                    if (i == (psychoEmotion1.size() - 1)) {
                        i = 0;
                    }
                }
            }
            i++;
        }
    }

    public void printEmotionSimilarities(String output) {
        String file = output + "emotionalSimilarities.txt";
        try {
            PrintWriter writer = new PrintWriter(file, "UTF-8");
            writer.println("\tUSER_ID1\t\t\tUSER_ID2\t\t\tEMOTIONAL_SIMILARITY");
            for (psychoEmotionalLevel emo : emotionLevel) {
                writer.println(emo.ID1 + "\t" + emo.ID2 + "\t" + emo.similarity);
            }
            writer.close();
        } catch (IOException e) {
        }
    }
}

