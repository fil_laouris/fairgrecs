package Documents;

/* =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 *                                                                        LIBRARIES
 * =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= */
import java.io.BufferedReader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/* =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 *                                                                 [ONTOLOGY HANDLER CLASS]
 * =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Class Name: Ontology Handler
 * Needs:
 *  1) XML Data (to Get Ontologies).
 *  2) Solr Core(to Save Ontologies).
 * Actions:
 *  1) Parsing XML data to get Ontologies (Category: EDAM, ICD-10).
 *  2) Save the ontologies in the corresponding Solr's Core.
 * =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= */
public class XML_Parser {

    /* =================================================================================================================
     * Method Name: xml_Parser_Solr_Save
     * Method Arguments:
     *   Arg1: XML string (URL of server that contain the xml file).
     * Method Return: Void.
     * Method Description:  Parse XML file and send each Term to Solr's Core base.
     * ================================================================================================================= */
    private Map<String, String> categoryLex;
    private Map<String, Set<String>> keywords;
    private Set<String> stopwords;

    public XML_Parser() {
        categoryLex = new HashMap<>();
        keywords = new HashMap<>();
        stopwords = new HashSet<>();
    }

    public void parseXMLFile(String file) {
        try {
            File inputFile = new File(file);
            // Create Document Factory
            DocumentBuilderFactory document_Factory = DocumentBuilderFactory.newInstance();
            // Create Document Builder
            DocumentBuilder document_Builder = document_Factory.newDocumentBuilder();
            // Create Document (by parsing our xml)
            Document document_XML = document_Builder.parse(inputFile);
            //Term Counter
            Integer term_counter = 0;

            NodeList section_NodeList = document_XML.getElementsByTagName("section");
            for (int i = 0; i < section_NodeList.getLength(); i++) {
                Node sec_Node = section_NodeList.item(i);
                String sectionName = sec_Node.getAttributes().getNamedItem("id").toString();
                sectionName = sectionName.replaceAll("id=", "");
                sectionName = sectionName.replaceAll("\"", "");
                NodeList children = sec_Node.getChildNodes();
                Set<String> setKey = new HashSet<>();
                for (int j = 0; j < children.getLength(); j++) {
                    Node diag = children.item(j);
                    addInLex(diag,setKey,sectionName);
                }
                keywords.put(sectionName, setKey);
            }

        } catch (SAXException ex) {
            Logger.getLogger(XML_Parser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XML_Parser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XML_Parser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addInLex(Node n, Set<String> setKey, String sectionName) {
        if (n.hasChildNodes()) {
            NodeList children = n.getChildNodes();
            for (int j = 0; j < children.getLength(); j++) {
                Node diag = children.item(j);
                addInLex(diag, setKey, sectionName);
            }
        }
        if (n.getNodeType() == Node.ELEMENT_NODE) {
            Element ele = (Element) n;
            if (ele.getTagName().equalsIgnoreCase("diag")) {
                String icd10_id = ele.getElementsByTagName("name").item(0).getTextContent();
                String icd10_name = ele.getElementsByTagName("desc").item(0).getTextContent();
                setKey.add(icd10_name);
                
                categoryLex.put(icd10_id, sectionName);
            }
        }
    }

    public Map<String, String> getCategoryLex() {
        return categoryLex;
    }

    public Map<String, Set<String>> getKeywords() {
        return keywords;
    }

    

}
/* =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 *                                                                       [CLASS STATUS]
 * =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Created by Michael Giannoulis on 19-03-2017.
 * Email: mikegian2008@hotmail.com
 * Version: Snapshot 2.0
 * =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= */
